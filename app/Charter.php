<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charter extends Model
{
    protected $fillable = [
        'name','img', 'description', 
    ];

    public function rate()
    {
        return $this->hasOne('App\Rate');
    }

    public function feature()
    {
        return $this->hasOne('App\BoatSpecification');
    }

    public function image()
    {
        return $this->hasOne('App\ImageBoat');
    }
}
