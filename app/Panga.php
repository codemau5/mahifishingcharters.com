<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Panga extends Model
{
    protected $fillable = [
        'name','img', 'description', 
    ];
    public function rate()
    {
        return $this->hasOne('App\Rate');
    }
    public function feature()
    {
        return $this->hasOne('App\BoatSpecification');
    }
    public function images()
    {
        return $this->hasMany('App\ImageBoat');
    }
}
