<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = [
        'panga_id','charter_id','half_day','full_day',
    ];
    public function panga(){
        return $this->belongsTo('App\Panga');
    }
    public function charter(){
        return $this->belongsTo('App\Charter');
    }
}
