<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageBoat extends Model
{
    public function panga(){
        return $this->belongsTo('App\Panga');
    }
    public function charter(){
        return $this->belongsTo('App\Charter');
    }
}
