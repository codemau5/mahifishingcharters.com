@extends('layouts.master')
@section('pangas-styles')
    <link href="https://fonts.googleapis.com/css2?family=Rowdies:wght@700&display=swap" rel="stylesheet">
   
    <style>
        .panga_box{
            padding: 1rem 0 3rem 0;
        }
        .panga_title{
            font-family: 'Rowdies', cursive!important;
            font-size: 3rem;
            letter-spacing: 1px;
            text-align: center;
        }

        .description_box{
            height: auto;
        }
        @media(min-width:1200px){
            .description_box{
                height: 80%;
                width: 100%
            }
            .panga_title{
                text-align: left;
            }
        }
        .panga_rates{
            width: 100%;
        }
        table {
            width: 800px;
            border-collapse: collapse;
            overflow: hidden;
            box-shadow: 0 0 20px rgba(0,0,0,0.1);
        }

        /* RATES */
        table {
            width: 800px;
            border-collapse: collapse;
            overflow: hidden;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
        }
        th,
        td {
            padding: 15px;
            background-color: rgba(255, 255, 255, 0.2);
            color: #fff;
        }
        th {
            text-align: left;
        }
        thead th {
            background-color: #55608f;
        }
        tbody tr:hover {
            background-color: rgba(255, 255, 255, 0.3);
        }
        tbody td {
            position: relative;
            color: #1b7482;
        }
        tbody td:hover:before {
            content: "";
            left: 0;
            right: 0;
            top: -9999px;
            bottom: -9999px;
            background-color: rgba(255, 255, 255, 0.2);
            z-index: -1;
        }
        .taxes{
            color: red;
        }
        .btn-book-now{
            width: 100%!important
        }
        .list-group-item{
            background-color: transparent!important;
        }
        .slider-for .slick-next{
            right: 10px;
        }
        .slider-for .slick-prev{
            position: absolute;
            left: 10px;
            z-index: 1000;
        }
        .charters-container{
                height: 80%;
            }
       @media(min-width:1024px){
            .charters-container{
                height: 95%;
            }
       }
        .charters-wrapper{
            overflow: hidden;
            position: -webkit-sticky;
            position: sticky;
            top: 0px;
            bottom: 0;
        }
        .m-none{
            display:none;
        }
        .m-button{
            max-width: 65px;
        }
        .m-text-head{
            font-size: 12px;
            padding: 0;
        }
        #boat_name{
            width: 85px!important;
        }
        @media(min-width: 1024px){
            .m-none{
                display:table-cell;
            }
            .m-button{
                max-width: 100%;
            }
            .m-text-head{
                font-size: inherit;
                padding: inherit;
            }
            #boat_name{
                width: 140px!important;
            }
        }
        

    </style>
@endsection

@section('content')
    <div class="container-fluid px-4 py-1">
        <div class="row">
            <div class="col-lg-10">
                @foreach ($charters as $charter)    
                <div class="panga_box">
                    <ul class="cards">
                        <h2 class="panga_title">{{strtoupper($charter->name)}} {{$charter->feets}}ft</h2>
                    </ul>
                
                        
                            
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="slider-for">
                                            <div>
                                                <img src="{{url($charter->img)}}" class="w-100" alt="">
                                            </div>
                                            <div>
                                                <img src="{{url($charter->img)}}" class="w-100" alt="">
                                            </div>
                                            <div>
                                                <img src="{{url($charter->img)}}" class="w-100" alt="">
                                            </div>
                                            <div>
                                                <img src="{{url($charter->img)}}" class="w-100" alt="">
                                            </div>
                                        </div>
                                        <div class="slider-nav">
                                            <div>
                                                <img src="{{url($charter->img)}}" class="w-100" alt="">
                                            </div>
                                            <div>
                                                <img src="{{url($charter->img)}}" class="w-100" alt="">
                                            </div>
                                            <div>
                                                <img src="{{url($charter->img)}}" class="w-100" alt="">
                                            </div>
                                            <div>
                                                <img src="{{url($charter->img)}}" class="w-100" alt="">
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="description_box">
                                            <div class="row mb-3">
                                                <div class="col-lg-6">
                                                    <ul class="list-group list-group-flush pb-2">
                                                        <li class="list-group-item p-1"><b>Fishing Gear:</b></li>
                                                        @php
                                                            $fishing_gears = explode(',', $charter->feature->fishing_gear);
                                                            $boat_features= explode(',',$charter->feature->boat_features);  
                                                        @endphp
                                            
                                                        @foreach ($fishing_gears as $fishing_gear)
                                                            <li class="list-group-item p-0">
                                                                {{$fishing_gear}}
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                <div class="col-lg-6">
                                                    <ul class="list-group list-group-flush pb-2">
                                                        <li class="list-group-item p-1"><b>Boat Features:</b></li>
                                                        @foreach ($boat_features as $boat_feature)
                                                            <li class="list-group-item p-0">
                                                                {{$boat_feature}}
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                    
                                            <p>{{$charter->description}}</p>
                                        </div>
                    
                                        <button class="btn-book-now w-100">BOOK {{strtoupper($charter->name)}} NOW</button>
                                    </div>
                                </div>
                                <div class="panga_rates">
                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="mx-auto tl-fixed w-100">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center m-text-head py-2">Duration</th>
                                                        <th class="text-center m-text-head py-2 m-none">Description</th>
                                                        <th class="text-center m-text-head py-2">Max Anglers</th>
                                                        <th class="text-center m-text-head py-2">Price</th>
                                                        <th class="text-center m-text-head py-2" id="boat_name">
                                                            {{$charter->name}}
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-center">
                                                            Full Day <br>
                                                            {{$charter->rate->duration_full_day}}
                                                        </td>
                                                        <td class="break-this text-center m-none">
                                                            {{$charter->rate->full_day_description ? $charter->rate->full_day_description : $charter->rate->default_full_day_description }}
                                                        </td>
                                                        <td class="text-center">
                                                            {{$charter->max_anglers}}
                                                        </td>
                                                        <td class="text-center">${{$charter->rate->full_day}}usd</td>
                                                        <td class="text-center">
                                                            <button class="m-button">Book full day</button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center">
                                                            Half Day <br>
                                                            {{$charter->rate->duration_full_day}}
                                                        </td>
                                                        <td class="break-this text-center m-none">
                                                            {{$charter->rate->half_day_description ? $charter->rate->half_day_description : $charter->rate->default_half_day_description}}
                                                        </td>
                                                        <td class="text-center">
                                                            {{$charter->max_anglers}}
                                                        </td>
                                                        <td class="text-center">${{$charter->rate->half_day}}usd</td>
                                                        <td class="text-center">
                                                            <button class="m-button">Book half day</button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="mx-auto tl-fixed w-100">
                                                <thead class="text-center">
                                                    <tr>
                                                        <th class="text-center p-0 bg-warning">EXTRA COSTS</th>
                                                    </tr>
                                                </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td class="text-center pt-1 pb-0">
                                                            <b>   FISHING LICENCE $18USD <br>
                                                                LIVE BAIT $30USD
                                                            </b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="taxes p-0">
                                                            <p class="text-center mb-2">mexican taxes 16% not included</p>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            
                        </div>
                    

            
                @endforeach
            </div>
            <div class="col-lg-2 w-100 bg-warning" style="min-height:100%">
                <h2>View Similar Boats</h2>
                <aside class="charters-container natural">
                    <div class="charters-wrapper">
                        @foreach ($charters as $charter)
                            <div class="row">
                                <a href="{{
                                    route('charter-id',$charter->id)
                                    }}">
                                    <div class="div col-lg-12">
                                        <p>{{$charter->name.' '.$charter->feets}}</p>
                                        <img src="{{asset($charter->img)}}" alt="" class="w-100">
                                        <p>max {{$charter->max_anglers}} anglers</p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </aside>
            </div>
        </div>
    </div>

@endsection

@section('footer-scripts')
    <script>
         $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            asNavFor: '.slider-nav',
            autoplay: true,
            autoplaySpeed: 3000,
            pauseOnHover: false
        });
        $('.slider-nav').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: true,
            arrows: false,
            centerMode: true,
            focusOnSelect: true
        });
        $('.charters-wrapper').slick({
            arrows: false,
            vertical: true,
            verticalSwiping: true,
            autoplay: true,
            autoplaySpeed: 2700,
            slidesToShow: 2,
            infinite:true,
            adaptiveHeight: true
        });
	
    </script>  


@endsection