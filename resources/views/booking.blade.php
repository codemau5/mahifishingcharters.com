@extends('layouts.master')
@section('pangas-styles')
    <link href="https://fonts.googleapis.com/css2?family=Rowdies:wght@700&display=swap" rel="stylesheet">
    <style>

        .boat_title{
            font-family: 'Rowdies', cursive!important;
            font-size: 3rem;
            letter-spacing: 1px;
        }

    </style>
@endsection

@section('content')
  
      <div class="panga_box">

        <ul class="cards">
            <h2 class="boat_title">{{strtoupper($boat->name)}} 23ft</h2>
        </ul>
        <div class="container-fluid px-4">
            <div class="row">
                <div class="col-lg-5">
                    <img src="{{url($boat->img)}}" class="w-100" alt="">
                </div>
                <div class="col-lg-7">
                    <div class="description_box">
                        <p>{{$boat->description}}</p>
                    </div>

                    <button class="btn-book-now">BOOK {{strtoupper($boat->name)}} NOW</button>
                </div>
            </div>
        </div>

      </div>

@endsection