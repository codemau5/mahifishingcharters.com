<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Mahi Fishing Charters</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css2?family=Rowdies:wght@300;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('css/slick-theme.css')}}"/>

        <link rel="stylesheet" href="{{url('/css/styles.css?=').time()}}">

        @yield('home-styles')
        @yield('pangas-styles')
    </head>
    <body>
        <header class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="container-fluid px-0">
                <nav class="navbar-expand-lg navbar-light bg-light pt-2 pb-0" id="menu-wrapper">

                   <div class="row mx-0">
                      <div class="col-lg-4 text-center icons">
                        <a target="_blank" href="https://www.facebook.com/mahisportfishing/">
                          <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a target="_blank" href="https://www.youtube.com/channel/UCkIfefTU5oeH3dtoqB0-dpA">
                          <i class="fa fa-youtube" aria-hidden="true"></i>
                        </a>
                      </div>
                     <div class="col-lg-4 text-center">
                        <a class="navbar-brand" href="/">
                          <img src="{{asset('img/Mahi-Sportfishing-logo-bco.png')}}?<?php rand(1,32000) ?>" alt="">
                        </a>  
                      </div>
                      <div class="col-lg-4 text-center icons">
                        <a target="_blank" href="https://www.instagram.com/mahisportfishing/">
                          <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a target="_blank" href="https://api.whatsapp.com/send?phone=526241610719&text=thanks%20for%20contact%20us,%20we'll%20send%20you%20a%20message%20in%20a%20few%20minutes!">
                          <i class="fa fa-whatsapp" aria-hidden="true"></i>
                        </a>
                      </div>
                      <button class="navbar-toggler px-0 col-2 offset-80" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <p style="
                            font-size: .5rem;
                            line-height: 0;
                            margin-bottom: 5px;
                            margin-top:3px;
                            color: #fff;
                            font-weight: 900;
                        ">MENU</p>
                        <span class="">
                          <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30"><path stroke="rgba(250, 242, 0, 1)" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2" d="M4 7h22M4 15h22M4 23h22"/></svg>
                        </span>
                      </button>
                   </div>
                      
                    <div class="container" id="menu-container">
                      <div class="row mx-0">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">

                          <ul class="navbar-nav mr-auto text-center">
                            <li class="nav-item">
                              <a class="nav-link" href="{{url('/')}}">HOME</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="{{url('/pangas')}}">PANGAS FLEET</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="{{url('book-now')}}">BOOK NOW</a>
                            </li>
                          </ul>

                          <ul class="navbar-nav ml-auto text-center">
                            <li class="nav-item">
                              <a class="nav-link" href="{{url('/charters')}}">CHARTERS</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="{{url('fishing-report')}}">FISHING REPORT</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="{{url('contact')}}">CONTACT</a>
                            </li>
                          </ul>

                          <ul class="mobile-icons text-center px-0">
                            <li>
                              <a href="https://www.facebook.com/mahisportfishing/" target="_blank" rel="noopener noreferrer">
                                <i class="fa fa-facebook mobil-icon" aria-hidden="true"></i>
                              </a>
                             <a href="https://www.youtube.com/channel/UCkIfefTU5oeH3dtoqB0-dpA" target="_blank" rel="noopener noreferrer">
                               <i class="fa fa-youtube mobil-icon" aria-hidden="true"></i>
                            </a>
                              <a href="https://www.instagram.com/mahisportfishing/" target="_blank" rel="noopener noreferrer">
                                <i class="fa fa-instagram mobil-icon" aria-hidden="true"></i>
                              </a>
                              <a href="https://api.whatsapp.com/send?phone=526241610719&text=hello%20I%20would%20like%20more%20information%20please" target="_blank" rel="noopener noreferrer">
                                <i class="fa fa-whatsapp mobil-icon" aria-hidden="true"></i>
                              </a>
                            </li>
                          </ul>
                          <div class="container-fluid justify-content-between text-white py-1 px-0 contact-info-mobil">
                              <div class="mail text-center">
                                <i class="fa fa-envelope pr-2"></i>info@mahifishincharters.com
                              </div>
                              <div class="tel text-center">
                                <i class="fa fa-phone pr-2"></i> USA-1456248746
                              </div>
                        </div>

                        </div>
                      </div>
                      <div class="container-fluid d-flex justify-content-between text-white py-1 px-0 contact-info">
                           <div class="mail text-center">
                              <i class="fa fa-envelope pr-2"></i>info@mahifishincharters.com
                           </div>
                           <div class="tel text-center">
                              <i class="fa fa-phone pr-2"></i> USA-1456248746
                           </div>
                      </div>
                    </div> <!-- #menu-container -->
                    
                  </nav>
            </div>
        </header>

       

            @yield('content')

            <footer id="footer">
                <div class="container-fluid brands">
                    <div class="row">
                        <div class="col-md-12">
                            
                        </div>
                    </div>
                </div>
                <div class="container-fluid footer">
                    <div class="container">
                        <div class="row wrapper">
                            <div class="col-md-12 text-center">
                            <img src="{{asset('img/Mahi-Sportfishing-logo-bco.png')}}"
                                     alt=""
                                     width="250px"
                                     height="auto"
                                     class="img-fluid my-4"
                                >
                            </div>
                            <div class="col-md-12 text-center">
                                <a href="{{url('/')}}">HOME</a> | <a href="{{url('pangas')}}">PANGAS FLEET</a> | <a href="{{url('/book-now')}}">BOOK NOW</a>| <a href="charters">CHARTERS</a> | <a href="{{url('fishing-report')}}">FISHING REPORT</a> | <a href="{{url('contact')}}">CONTACT</a>
                            </div>
                            <div class="col-md-12 text-center my-2">
                                Phone: <a href="tel:+526241610719">(624) 161 0719 </a> <br>
                                Jose ceseña:<a href="tel:+5216241575157"> (624) 161 0719</a><br>
                                Copyright &copy; 2020 Mahi Fishing Charters.  <br>All rights reserved.
                               
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
      
              <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
              <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
              <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
              <script type="text/javascript" src="{{asset('js/slick.min.js')}}"></script>
      
              <script>
                $(document).ready(function(){
                  $('.home-slider').slick({
                    autoplay: true,
                    pauseOnHover: false
                  });
                });
              </script>
              @yield('footer-scripts')
      
          </body>
      </html>
      