@extends('layouts.master')

@section('home-styles')
    <style>
        .slick-next {
              display: none!important;
          }
        .card_image {
            background-image: url("{{ url('/img/slider/slide_1.jpeg') }}");
        }

  </style>
@endsection

@section('content')


<div class="home-slider">

  <div>
    <img src="{{asset('img/slider/slide_1.jpeg')}}" class="w-100" alt="">
  </div>
  <div>
    <img src="{{asset('img/slider/slide_1.jpeg')}}" class="w-100" alt="">
  </div>
  <div>
    <img src="{{asset('img/slider/slide_1.jpeg')}}" class="w-100" alt="">
  </div>

</div>
    

    <div class="main py-5">
      <h1 class="home-title">OUR FLEET</h1>
      <ul class="cards">
        <li class="cards_single">
          <div class="card">
            <h2 class="card_title py-3">SUPER PANGA 23'</h2>
            <div class="card_image"></div>
            <div class="card_content">
                <a href="{{url('/pangas')}}">
                  <button class="btn btn-view-fleet">
                    VIEW FLEET
                  </button>
                </a>
                <ul class="card_text py-3">
                 <li>max 3 anglers.</li>
                 <li>GPS, fish finder & radio.</li>
                </ul>
                <a href="{{url('/booking')}}">
                  <button class="btn-book-now card_btn"><b>BOOK NOW</b></button>
                </a>
            </div>
          </div>
        </li>
      </ul>
    </div>


    <div class="main py-5">
      <h1 class="home-title">OUR CHARTERS</h1>
      <ul class="cards">
        @foreach ($charters as $charter)
        <li class="cards_item">
            <div class="card">
                <h2 class="card_title pt-3 pb-2">{{$charter->name}}</h2>
                <div class="card_image" style="background-image: url('{{ url($charter->img) }}')!important;"></div>
                <div class="card_content">
                <p class="card_text">
                    {{ $charter->description }}
                </p>
                {{-- <a href="{{url("/charter\/")}}{{ $charter->id }}"> --}}
                <a href="{{route('charter-id',$charter->id)}}">
                  <button class="btn-book-now card_btn"><b>BOOK NOW</b></button>
                </a>
                </div>
            </div>
            
        </li>
    @endforeach
    </ul>
    </div>
    
@endsection