<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

use App\Panga;
use App\Charter;
use App\Rate;
use App\ImageBoat;
use App\BoatSpecification;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $charters=  DB::table('charters')->get();

    return view('home')->with('charters',$charters);
});


Route::get('/pangas', function () {
   

    $pangas=  Panga::get();
    $charters= DB::table('charters')->get();

    return view('pangas', compact('pangas','charters'));
});

Route::get('/charters', function () {
    $charters= Charter::get();
    return view('charters')->with('charters',$charters);
});

Route::get('charter/{id}', function ($id) {
    $charter= Charter::find($id);
    $charters= Charter::get();
    return view('charter',compact('charter','charters'));
})->name('charter-id');

Route::get('user/{id}', function ($id) {
    return 'User '.$id;
});
